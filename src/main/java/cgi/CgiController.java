package cgi;

import cgi.model.Information;
import cgi.javatopython.JavaToPythonUtil;
import cgi.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:8081/")
@RestController
public class CgiController {


    private final StorageService storageService;

    @Autowired
    public CgiController(StorageService storageService) {
        this.storageService = storageService;
    }

    @PostMapping("/file")
    public Map<String,String> handleFileUpload(@RequestParam("file") MultipartFile file,
                                               RedirectAttributes redirectAttributes) {


        Map<String, String> returnRequest = new HashMap<>();
        Information information = new Information();
        String name = StringUtils.cleanPath(file.getOriginalFilename());


        int width = 0;
        int height = 0;
        String format = "None";

        storageService.store(file);


        try {
            ImageInputStream iis = ImageIO.createImageInputStream(new File("src\\main\\java\\cgi\\images\\" + name));
            Iterator iter = ImageIO.getImageReaders(iis);
            if (iter.hasNext()) {
                ImageReader reader = (ImageReader) iter.next();
                format = reader.getFormatName();
                iis.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }





        try
        {
            BufferedImage image = ImageIO.read(new File("src\\main\\java\\cgi\\images\\" + name));
            width = image.getWidth();
            height = image.getHeight();

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }






        information.setFileName(name);
        information.setFormat(format);
        information.setResolution(width + "x" + height);
        information.setClassification(JavaToPythonUtil.getClassificationRandom(width,height));


        returnRequest.put("name", information.getFileName());
        returnRequest.put("format", information.getFormat());
        returnRequest.put("resolution", information.getResolution());
        returnRequest.put("class", information.getClassification());

        return returnRequest;
    }


}