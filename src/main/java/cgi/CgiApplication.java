package cgi;

import cgi.storage.StorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class CgiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CgiApplication.class, args);
    }



}
