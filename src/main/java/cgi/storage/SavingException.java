package cgi.storage;

import java.io.IOException;

public class SavingException extends IOException {

    public SavingException(String errorMessage){
        super(errorMessage);
    }

}

