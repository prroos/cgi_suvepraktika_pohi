package cgi.javatopython;




import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;


public class JavaToPythonUtil {

    public static  String getClassificationRandom(int width, int height){
        String classification = "None";
        try {
            String command = "python src\\main\\java\\cgi\\javatopython\\GetRandomClassification.py";


            Process process = Runtime.getRuntime().exec(command + " " + (width + height));

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            classification = reader.readLine();
            reader.close();

        } catch(IOException e) {
            e.printStackTrace();
        }
        return classification;
    }




}